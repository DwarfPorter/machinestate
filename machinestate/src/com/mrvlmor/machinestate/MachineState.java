package com.mrvlmor.machinestate;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import android.util.Log;


// pattern machine state
public class MachineState<TEvents extends Enum<TEvents>> implements IEventSink<TEvents> {

	private IEventSink<TEvents> currentState;
	private Stack<IEventSink<TEvents>> prevStates = new Stack<IEventSink<TEvents>>(); 
	private boolean usePrevState;
	
	private Map<IEventSink<TEvents>, Map<TEvents, IEventSink<TEvents>>> edges = new HashMap<IEventSink<TEvents>, Map<TEvents, IEventSink<TEvents>>>();
	
	IHaveEngine haveEngine;
	
	public MachineState(IHaveEngine haveEngine){
		this(haveEngine, true);
	}
	
	public MachineState(IHaveEngine haveEngine, boolean usePrevState){
		this.haveEngine = haveEngine;
		this.usePrevState = usePrevState;
	}
	
	// add sign for state
	public void addEdge(IEventSink<TEvents> source, TEvents event, IEventSink<TEvents> target) {
		Map<TEvents, IEventSink<TEvents>> row = edges.get(source);	// get table of states in depends for current state
		if (row == null) {	// if nothing then this is new state
			row = new EnumMap<TEvents, IEventSink<TEvents>>(getEventClass(event));	// create new table of state
			edges.put(source, row);							// add new state to this
		}
		row.put(event, target);								// and add sign
	}

	private Class<TEvents> getEventClass(TEvents event) {
		return (Class<TEvents>) event.getClass();
	}
		
	public void setState(IEventSink<TEvents> state){
		setStateCore(state, true);
	}
	
	public boolean setPrevState(){
		Log.d("MachineState", "PRevState");
		if (prevStates.empty()){ 
			//System.exit(0);
			return false;
		}
		setStateCore(prevStates.pop(), false);
		return true;
	}
	
	public void onResume() {
		if (currentState == null) return;
		currentState.onResume();
	}

	public void onPause() {
		if (currentState == null) return;
		currentState.onPause();
	}
	
	public boolean onKeyPressed(int keyCode){
		if (currentState == null) return false;
		return currentState.onKeyPressed(keyCode);
	}
	
	
	
	private void setStateCore(IEventSink<TEvents> state, boolean isPushPRevState){
		if (state == null) return;
		if (state.equals(currentState))
			return;
		
		if(isPushPRevState && usePrevState && currentState != null){
			prevStates.push(currentState);
		}
		closeState();
		
		currentState = state;				
		BaseStateScene scene = currentState.createScene(); // create scene
		haveEngine.getEngine().setScene(scene); // set current scene
		
	}

	// when sign is got set new state
	public void castEvent(TEvents event) {
		try {
			setState(edges.get(currentState).get(event));	// new state
		} 
		catch (NullPointerException e) {
			//throw new IllegalStateException("Edge is not defined");
			;
		}
	}
	
	public void closeState(){
		if (currentState == null) return;
		currentState.closeState();
	}

	public BaseStateScene createScene() {
		if (currentState == null) return null;
		return currentState.createScene();
	}

	public boolean canReturn() {
		if (currentState == null) return false;
		return currentState.canReturn();
	}
	
	public IEventSink<TEvents> getCurrentState(){
		return currentState;
	}


	
}
