package com.mrvlmor.machinestate;

import org.andengine.ui.activity.BaseGameActivity;

public abstract class BaseState<TEvents extends  Enum<TEvents>> implements IEventSink<TEvents> {

	private IEventSink<TEvents> machineState;
	private BaseGameActivity activity;
	protected BaseStateScene scene;
	private boolean isStarted = false;
	
	public BaseState (IEventSink<TEvents> machineState, BaseGameActivity activity){
		this.machineState = machineState;
		this.activity = activity;
	}
	
	
	public void castEvent(TEvents event) {
		machineState.castEvent(event);
	}

	public void setState(IEventSink<TEvents> state) {

		if (state == null) return;
		if (state.equals(this))	return;

		setScene(state);
	}

	public void setScene(IEventSink<TEvents> state){
		machineState.setState(state); //machine state!
	}
	
	public abstract BaseStateScene createScene(BaseGameActivity activity);
	
	public BaseStateScene createScene(){
		scene = createScene(activity); // create scene
		startScene();			// start scene
		return scene;
	}
	
	public boolean setPrevState() {
		return machineState.setPrevState();
	}
	
	public boolean onKeyPressed(int keyCode){
		return scene.onKeyPressed(keyCode);
	}
	
	public void onPause(){
		scene.onPause();
	}
	
	public void onResume(){
		scene.onResume();
	}

	public boolean canReturn(){
		return scene.canReturn();
	}
	
	public void startScene(){
		if (scene != null){
			if (!isStarted){
				scene.startScene();
				isStarted = true;
			}
			scene.openState();
		}
	}
	
	public void openState(){
		if(scene != null){
			scene.openState();
		}
	}
	
	public void closeState() {
		if(scene != null){
			scene.closeState();
		}
	}
}
