package com.mrvlmor.machinestate;

public interface IHaveSizeCamera {
	public int getWidthCamera();
	public int getHeightCamera();
}
