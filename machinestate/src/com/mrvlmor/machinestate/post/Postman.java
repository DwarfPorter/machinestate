package com.mrvlmor.machinestate.post;

import java.util.ArrayList;
import java.util.List;

public class Postman {

	private static List<IMail> mails = new ArrayList<IMail>();
	
	private Object syncroot = new Object();
	
	public Postman() {}
	
	public void sendMail(IMail mail){
		synchronized (syncroot) {
			mails.add(mail);
		}
	}
	
	public IMail receiveMail(String addressMail){
		IMail found = search(addressMail);
		if (found == null) return null;
		synchronized (syncroot) {
			mails.remove(found);
		}
		return found;
	}
	
	private IMail search(String addressMail){
		for(IMail mail:mails){
			if(mail.getAddress().equals(addressMail)){
				return mail;
			}
		}
		return null;
	}
	
}
