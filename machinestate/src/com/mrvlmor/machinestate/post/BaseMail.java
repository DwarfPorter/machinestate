package com.mrvlmor.machinestate.post;


public class BaseMail implements IMail {

	private Object content;
	private String address;
	
	public BaseMail(Object mailContent, String mailAddress) {
		content = mailContent;
		address = mailAddress;
	}

	public String getAddress(){
		return address;
	}

	public Object getMail() {
		return content;
	}

}
