package com.mrvlmor.machinestate.post;

// mail between two objects
public interface IMail
{
	String getAddress();
	Object getMail();
}
