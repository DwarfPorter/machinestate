package com.mrvlmor.machinestate;


public abstract class BaseSimpleScene<TEvents extends Enum<TEvents>> extends BaseStateScene implements IEventSink<TEvents> {
	
	private IEventSink<TEvents> state; 

	public BaseSimpleScene(IEventSink<TEvents> state)
	{
		this.state = state;
	}
	
	public void castEvent(TEvents event){
		state.castEvent(event);
	}
	public void setState(IEventSink<TEvents> state){
		state.setState(state);
	}
	
	public BaseStateScene createScene(){
		startScene();
		return this;
	}
	
	public boolean setPrevState(){
		return state.setPrevState();
	}
	
}
