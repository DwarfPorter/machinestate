package com.mrvlmor.machinestate;

import org.andengine.engine.Engine;

public interface IHaveEngine {
	public Engine getEngine();
}
