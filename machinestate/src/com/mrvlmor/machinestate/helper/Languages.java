package com.mrvlmor.machinestate.helper;

import java.util.Locale;

import android.content.res.Configuration;
import android.content.res.Resources;

public class Languages {
	
	public final static String LANGUAGE_ENGLISH = "en";
	public final static String LANGUAGE_RUSSIAN = "ru";
	public final static String LANGUAGE_GERMAN = "de";
	public final static String LANGUAGE_FRENCH = "fr";
	public final static String LANGUAGE_SPAIN = "es";
	public final static String LANGUAGE_CHINA = "zh";
	public final static String LANGUAGE_KOREAN = "ko";
	public final static String LANGUAGE_TURKEY = "tr";
	public final static String LANGUAGE_ARABIC = "ar";
	public final static String LANGUAGE_PORTUGAL = "pt";
	
	
	protected Resources localResources;
	
	// define Language string and on each value string assing string
	public Languages(Resources baseResources, String lang) {
		Configuration config = new Configuration(baseResources.getConfiguration());
		
		if (lang.equals(LANGUAGE_RUSSIAN))
			config.locale = new Locale(LANGUAGE_RUSSIAN);
		else if (lang.equals(LANGUAGE_GERMAN)) 
			config.locale = Locale.GERMAN;
		else if (lang.equals(LANGUAGE_FRENCH))
			config.locale = Locale.FRENCH;
		else if (lang.equals(LANGUAGE_SPAIN))
			config.locale = new Locale(LANGUAGE_SPAIN);
		else if (lang.equals(LANGUAGE_CHINA))
			config.locale = Locale.CHINESE;
		else if (lang.equals(LANGUAGE_KOREAN))
			config.locale = Locale.KOREAN;
		else if (lang.equals(LANGUAGE_TURKEY))
			config.locale = new Locale(LANGUAGE_TURKEY);
		else if (lang.equals(LANGUAGE_ARABIC))
			config.locale = new Locale(LANGUAGE_ARABIC);
		else if (lang.equals(LANGUAGE_PORTUGAL))
			config.locale = new Locale(LANGUAGE_PORTUGAL);
		else	
			config.locale = Locale.ENGLISH;
		
		localResources = new Resources(baseResources.getAssets(), baseResources.getDisplayMetrics(), config);
		
	}	
	
	public String getString(int idString){
		return localResources.getString(idString);
	}
	
	
}
