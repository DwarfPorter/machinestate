package com.mrvlmor.machinestate.helper;

import org.andengine.engine.Engine;
import org.andengine.engine.options.EngineOptions;

import org.andengine.util.time.TimeConstants; 

public class FlexibleFPSEngine extends Engine {

	 private long mPreferredFrameLengthNanoseconds;
	 private int mFramesPerSecond;
	 
	 public FlexibleFPSEngine(final EngineOptions pEngineOptions, final int pFramesPerSecond) {
	  super(pEngineOptions);
	  mFramesPerSecond = pFramesPerSecond;
	  this.mPreferredFrameLengthNanoseconds = TimeConstants.NANOSECONDS_PER_SECOND / pFramesPerSecond;
	 } 
	 
	 @Override
	 public void onUpdate(final long pNanosecondsElapsed) throws InterruptedException {
	    final long preferredFrameLengthNanoseconds = this.mPreferredFrameLengthNanoseconds;
	    final long deltaFrameLengthNanoseconds = preferredFrameLengthNanoseconds - pNanosecondsElapsed;
	    if(deltaFrameLengthNanoseconds <= 0) {
	    	super.onUpdate(pNanosecondsElapsed);
	    } else {
	    	final int sleepTimeMilliseconds = (int) (deltaFrameLengthNanoseconds / TimeConstants.NANOSECONDS_PER_MILLISECOND);
	    	Thread.sleep(sleepTimeMilliseconds);
	    	super.onUpdate(pNanosecondsElapsed + deltaFrameLengthNanoseconds);
	    }
	 } 
	 
	 public void setFramesPerSecond (final int pFramesPerSecond) {
		 if (mFramesPerSecond == pFramesPerSecond) return;
		 mFramesPerSecond = pFramesPerSecond;
		 this.mPreferredFrameLengthNanoseconds = TimeConstants.NANOSECONDS_PER_SECOND / pFramesPerSecond;
	 }
	
}
