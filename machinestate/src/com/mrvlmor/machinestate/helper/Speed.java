package com.mrvlmor.machinestate.helper;

public class Speed {
	
	private static float VELOCITY = 1f;

	public static long getVelocity  (long duration){
		return (long) (duration * VELOCITY);
	}
	
	public static float getVelocity  (float duration){
		return (float) (duration * VELOCITY);
	}
	
	public static int getVelocity  (int duration){
		return (int) (duration * VELOCITY);
	}
	
	public static void setVelocity(float vel){
		VELOCITY = vel;
	
	}
}
