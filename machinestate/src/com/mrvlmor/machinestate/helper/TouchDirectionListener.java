package com.mrvlmor.machinestate.helper;

import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;

public class TouchDirectionListener implements IOnSceneTouchListener{
	private float firstX;
	private float firstY;
	private float nowX;
	private float nowY;
	
	private IMove movingField;
	
	public TouchDirectionListener(IMove movingField){
		this.movingField = movingField;
	}
	
	public boolean onSceneTouchEvent(Scene pScene,
			TouchEvent pSceneTouchEvent) {
		if (pSceneTouchEvent.isActionDown()) {
			firstX = pSceneTouchEvent.getX();
			firstY = pSceneTouchEvent.getY();
			return true;
		}
		if (pSceneTouchEvent.isActionMove()){
			nowX = pSceneTouchEvent.getX();
			nowY = pSceneTouchEvent.getY();
			if (Math.abs(Math.abs(firstX - nowX) - Math.abs(firstY - nowY)) < 30)
			{
				return true;
			}
			Direction direct;
			if (Math.abs(firstX - nowX) < Math.abs(firstY - nowY)){
				if (firstY > nowY){
					direct = Direction.UP;
				}
				else{
					direct = Direction.DOWN;
				}
			}else{
				if (firstX > nowX){
					direct = Direction.LEFT;
				}
				else{
					direct = Direction.RIGHT;
				}
			}
			movingField.move(direct);
			return true;
		}
		if (pSceneTouchEvent.isActionUp()) {
			movingField.move(Direction.STOP);
			return true;
		}
		return false;
	}
}