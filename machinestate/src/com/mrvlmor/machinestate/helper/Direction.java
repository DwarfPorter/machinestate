package com.mrvlmor.machinestate.helper;

public enum Direction {
	STOP,
	UP,
	DOWN,
	RIGHT,
	LEFT
}
