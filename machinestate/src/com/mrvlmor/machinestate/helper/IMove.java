package com.mrvlmor.machinestate.helper;

public interface IMove {
	public void move(Direction direct);
}
