package com.mrvlmor.machinestate;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.ui.activity.BaseGameActivity;

public class SplashScene extends Scene {

    public ITextureRegion splash_region;
    private BitmapTextureAtlas splashTextureAtlas;
    
	private Sprite splash;
	
	private String splashFileName = "gfx/game/splash.png";
	
	public SplashScene(BaseGameActivity activity, IHaveSizeCamera sizeCamera)
	{
		loadSplashScene(activity);
		startScene(activity, sizeCamera);
	}
	
	public SplashScene(BaseGameActivity activity, IHaveSizeCamera sizeCamera, String splashFileName)
	{
		this.splashFileName = splashFileName;
		loadSplashScene(activity);
		startScene(activity, sizeCamera);
	}
	
	public void startScene(BaseGameActivity activity, IHaveSizeCamera sizeCamera) {
		splash = new Sprite(0, 0, splash_region, activity.getVertexBufferObjectManager())
		{
		    @Override
		    protected void preDraw(GLState pGLState, Camera pCamera) 
		    {
		       super.preDraw(pGLState, pCamera);
		       pGLState.enableDither();
		    }
		};
		         

		splash.setPosition((sizeCamera.getWidthCamera() - splash.getWidth()) / 2 , (sizeCamera.getHeightCamera() - splash.getHeight()) / 2);
		attachChild(splash);
	}

	public void disposeScene() {
	    splash.detachSelf();
	    splash.dispose();
    	splashTextureAtlas.unload();
    	splash_region = null;
	    this.detachSelf();
	    this.dispose();
	}

    
    // load splash resource
	private void loadSplashScene(BaseGameActivity activity) {
		//BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/game/");
		splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 512, TextureOptions.BILINEAR);
		splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, splashFileName, 0, 0);
		splashTextureAtlas.load();
	}
	 
}
