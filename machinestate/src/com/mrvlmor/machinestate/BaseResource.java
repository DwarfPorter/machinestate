package com.mrvlmor.machinestate;

import org.andengine.ui.activity.BaseGameActivity;

public abstract class BaseResource<TActivity extends BaseGameActivity> {
	private TActivity activity;
	
	private boolean init = false;
	
	private static Object syncroot = new Object();
	
	public BaseResource(TActivity activity){
		this.activity = activity;
		load();
	}

	public TActivity getActivity(){
		return activity;
	}

	protected abstract void loadCore();
	
	public void load(){
		synchronized (syncroot) {	
			if (init) return;
			loadCore();
			init = true;
		}
	}

	
}
