package com.mrvlmor.machinestate;

import org.andengine.entity.scene.Scene;

public abstract class BaseStateScene extends Scene {

	
	public abstract void startScene();
	
	public void closeState(){
	}
	
	public void openState(){
	}
	
	public void onResume(){}
	public void onPause(){}
	
	public abstract boolean onKeyPressed(int keyCode);

	public boolean canReturn() {
		return true;
	}
	
}
