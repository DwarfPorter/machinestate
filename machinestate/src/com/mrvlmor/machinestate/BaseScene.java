package com.mrvlmor.machinestate;

import org.andengine.ui.activity.BaseGameActivity;

import com.mrvlmor.machinestate.IEventSink;

public abstract class BaseScene<TResource extends BaseResource<? extends BaseGameActivity>, TEvents extends Enum<TEvents>> extends BaseSimpleScene<TEvents> {
	
	private TResource resource;

	public BaseScene(TResource resource, IEventSink<TEvents> state)
	{
		super(state);
		this.resource = resource;
	}

	public abstract void startScene();
	
	protected TResource getResource(){
		return resource;
	}
	
	protected BaseGameActivity getActivity(){
		return getResource().getActivity();
	}
	
	
}
