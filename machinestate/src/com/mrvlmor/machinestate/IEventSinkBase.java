package com.mrvlmor.machinestate;

public interface IEventSinkBase {
	public boolean onKeyPressed(int keyCode);
	public void onResume();
	public void onPause();
}
