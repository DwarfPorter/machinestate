package com.mrvlmor.machinestate;



public interface IEventSink<TEvents extends Enum<TEvents>> extends IEventSinkBase {
	public void castEvent(TEvents event);
	public void setState(IEventSink<TEvents> state);
	public void closeState();
	public BaseStateScene createScene();
	public boolean setPrevState();
	public boolean canReturn();
}
